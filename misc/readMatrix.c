#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>


int main()
{
    std::fstream in("in.txt");
    std::string line;
    std::vector<std::vector<float>> v;
    int i = 0;

    while (std::getline(in, line))
    {
        float value;
        std::stringstream ss(line);

        v.push_back(std::vector<float>());

        while (ss >> value)
        {
            v[i].push_back(value);
        }
        ++i;
    }

    int rows = v.size();
    int cols = (v[0]).size();
    for(i = 0; i < rows-1  ;++i){
        for(int j = 0; j<4; ++j){
            std::cout << v[i][j] << " ";
        }
        std::cout << std::endl;
    }


}
