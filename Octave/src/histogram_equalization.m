function salida = histogram_equalization(imagen,reduccion)

I = imread(imagen);
I = imresize(I, reduccion);

a = histeq(I(:,:,1));
b = histeq(I(:,:,2));
c = histeq(I(:,:,3));

salida(:,:,1) = a;
salida(:,:,2) = b;
salida(:,:,3) = c;