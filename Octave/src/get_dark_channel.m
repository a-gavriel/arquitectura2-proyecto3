# This function looks for the lowest value of pixels, the dark ones
# After this, it maps each patch to an image of the same dimension with each minimum value

# win_size tells the square area that covers the image where it looks for the minimum value pixel

function dark_channel = get_dark_channel(image, win_size)

[m, n, ~] = size(image);

pad_size = floor(win_size/2);

padded_image = padarray(image, [pad_size pad_size], Inf);

dark_channel = zeros(m, n); 

for j = 1 : m
    for i = 1 : n
        patch = padded_image(j : j + (win_size-1), i : i + (win_size-1), :);

        dark_channel(j,i) = min(patch(:));
     end
end

end