# The two images MUST match sizes

function salida = ajuste_hsb_b(imagen,brillo)

imagen_hsv = rgb2hsv(imagen);

imagen_hsv(:,:,3) = imagen_hsv(:,:,3)*brillo;

imagen_rgb = hsv2rgb(imagen_hsv);

salida = dehaze_fast(imagen_rgb, 1, 5);

endfunction