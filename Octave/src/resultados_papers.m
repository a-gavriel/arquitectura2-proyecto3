clear
clc

pkg load image
pkg load image

imagen = 'dark_channel_prior.png';
reduccion = 1; #0.3
I = imread (imagen);

[ssr msr msrcr] = retinex(imagen,reduccion);
histograma = histogram_equalization(imagen,reduccion);

I = imresize(I, reduccion);
imagen = double(I)/255;
dark_channel_prior = dehaze_fast(imagen, 1, 5);

figure

subplot(2,3,1);imshow(I);title('Entrada')
subplot(2,3,2);imshow(ssr);title('SSR')
subplot(2,3,3);imshow(msr);title('MSR')
subplot(2,3,4);imshow(msrcr);title('MSRCR')
subplot(2,3,5);imshow(histograma);title('Histogram Equalization')
subplot(2,3,6);imshow(dark_channel_prior);title('Dark Channel Prior')