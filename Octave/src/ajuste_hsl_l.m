# The two images MUST match sizes

function salida = ajuste_hsl_l(imagen,luminosidad)

imagen_hsl = rgb2hsl(imagen);

imagen_hsl(:,:,3) = imagen_hsl(:,:,3)*luminosidad;

imagen_rgb = hsl2rgb(imagen_hsl);

salida = dehaze_fast(imagen_rgb, 1, 5);

endfunction