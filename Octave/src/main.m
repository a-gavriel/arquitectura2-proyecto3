clear
clc

pkg load image
pkg load image

imagen = 'finca_3.jpg';
reduccion = 0.1; #0.3
brillo_1 = 0.75;
brillo_2 = 0.5;
sigma_1 = 5;
sigma_2 = 5;
I = imread (imagen);

[ssr msr msrcr] = retinex(imagen,reduccion);
histograma = histogram_equalization(imagen,reduccion);

I = imresize(I, reduccion);
imagen = double(I)/255;
dark_channel_prior = dehaze_fast(imagen, 1, 5);

# Modules that changes the brightness and luminosity of the scene previously to the dark channel prior
dark_channel_prior_b1 = ajuste_hsb_b(imagen,brillo_1);
dark_channel_prior_b2 = ajuste_hsb_b(imagen,brillo_2);
dark_channel_prior_l1 = ajuste_hsl_l(imagen,brillo_1);
dark_channel_prior_l2 = ajuste_hsl_l(imagen,brillo_2);

figure

subplot(2,3,1);imshow(I);title('Entrada')
subplot(2,3,2);imshow(ssr);title('SSR')
subplot(2,3,3);imshow(msr);title('MSR')
subplot(2,3,4);imshow(msrcr);title('MSRCR')
subplot(2,3,5);imshow(histograma);title('Histogram Equalization')
subplot(2,3,6);imshow(dark_channel_prior);title('Dark Channel Prior')

figure

subplot(2,3,1);imshow(I);title('Entrada')
subplot(2,3,2);imshow(dark_channel_prior);title('Dark Channel Prior')
subplot(2,3,3);imshow(dark_channel_prior_b1);title(strcat( 'Dark Channel Prior con brillo previo de:',num2str(brillo_1)))
subplot(2,3,4);imshow(dark_channel_prior_b2);title(strcat( 'Dark Channel Prior con brillo previo de:',num2str(brillo_2)))
subplot(2,3,5);imshow(dark_channel_prior_l1);title(strcat( 'Dark Channel Prior con luminosidad previa de:',num2str(brillo_1)))
subplot(2,3,6);imshow(dark_channel_prior_l2);title(strcat( 'Dark Channel Prior con luminosidad previa de:',num2str(brillo_2)))