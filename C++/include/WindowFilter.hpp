#ifndef  WINDOW_FILTER_HPP
#define  WINDOW_FILTER_HPP

#include <cstdlib>
#include <iostream>

#include <string>

#include <AnpiConfig.hpp>

#include <fstream>
#include <Matrix.hpp>
#include <Exception.hpp>

#include "Utilities.hpp"
#include "omp.h"
#include <algorithm>

namespace anpi
{

template <typename T>
void window_sum(const Matrix<T> &image, size_t r,Matrix<T> &sum_img ){
  size_t h = image.rows();
  size_t w = image.cols();

  sum_img.allocate(h,w);
  sum_img.fill(T(0));

  Matrix<T> im_cum(h,w);
  cumsumRow(image,im_cum);
  Matrix<T> temp1 , temp2, temp3;
  

  // Y axis
  copyWindow(sum_img, 0 , r, 0, w-1 , im_cum, r, r+r, 0, w-1 );
  
  getWindow( im_cum, 2*r+1, h-1, 0, w-1, temp1 );  
  getWindow( im_cum, 0, h-2*r-2, 0, w-1, temp2 );

  if (temp1.cols() != 0 && temp2.cols() !=0){
    temp1 -= temp2;
    copyWindow(sum_img, r+1,h-r-1, 0, w-1, temp1, 0, temp1.rows()-1,0,temp1.cols()-1);
  }


  getWindow(im_cum,h-1,h-1,0,w-1 , temp2);
  repmat(temp2,r,1,temp3);
  getWindow(im_cum, h-2*r-1, h-r-2,0,w-1 ,temp1 );

  if (temp1.cols() != 0 ){
    temp3 -= temp1;
    copyWindow(sum_img,h-r,h-1,0,w-1, temp3, 0, temp3.rows()-1, 0, temp3.cols()-1 );
  }
  
  // X axis
  cumsumCol(sum_img,im_cum);


  copyWindow(sum_img , 0,h-1,0,r , im_cum, 0 , h-1, r,r+r  );
  

  getWindow( im_cum, 0,h-1,2*r+1, w-1, temp1 );
  getWindow( im_cum, 0,h-1,0,w-2*r-2, temp2 );
  if (temp1.cols() != 0 && temp2.cols() !=0){
    temp1 -= temp2;
    copyWindow(sum_img,0 , h-1,w-r ,w-1,  temp1, 0, temp1.rows()-1,0,temp1.cols()-1);
  }

  getWindow(im_cum, 0,h-1,w-1,w-1  , temp2);
  repmat(temp2,1,r,temp3);
  getWindow(im_cum, 0,h-1,w-2*r-1,w-r-2 ,temp1 );
  if (temp1.cols() != 0 && temp3.cols() !=0){
    temp3 -= temp1;
    copyWindow(sum_img, 0,h-1,w-r,w-1 , temp3, 0, temp3.rows()-1, 0, temp3.cols()-1 );
  }

}









} // namespace anpi

#endif // WINDOW_FILTER_HPP
