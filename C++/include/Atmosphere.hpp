#ifndef ATMOSPHERE_HPP
#define ATMOSPHERE_HPP

#include <cstdlib>
#include <iostream>

#include <string>

#include <AnpiConfig.hpp>
#include <Spline.hpp>
#include <fstream>
#include <Matrix.hpp>
#include <Exception.hpp>
#include "Solver.hpp"
#include "Utilities.hpp"
#include "omp.h"
#include "PlotPy.hpp"
#include <algorithm>

namespace anpi
{


template <typename T>
void get_atmosphere(const Matrix<T> &imageR, const Matrix<T> &imageG, const Matrix<T> &imageB,
 const Matrix<T> &darkchChannel,
 Matrix<T> &atmosphere){

  size_t m = imageR.rows();
  size_t n = imageR.cols();
  size_t n_pixels = m*n;

  size_t n_search_pixels = n_pixels / 100;


  std::vector<T> dark_vec;
  std::vector<size_t> indices;
  //Converts the matrix into 2 vectors, One of values, One of indices
  vectorizeIndex(darkchChannel, dark_vec,indices);

  std::vector<T> image_vectR, image_vectG, image_vectB;
  vectorize(imageR, image_vectR);
  vectorize(imageG, image_vectG);
  vectorize(imageB, image_vectB);

  
  sort(dark_vec, indices,1);

  T accumulatorR = T(0), accumulatorG= T(0), accumulatorB= T(0);

  for(size_t k=0;k<n_search_pixels; ++k){
    accumulatorR= accumulatorR + image_vectR[  indices[k] ];
    accumulatorG= accumulatorG + image_vectG[  indices[k] ];
    accumulatorB= accumulatorB + image_vectB[  indices[k] ];
  }
  
  atmosphere.allocate(1,3);
  atmosphere(0,0) = accumulatorR / T(n_search_pixels);
  atmosphere(0,1) = accumulatorG / T(n_search_pixels);
  atmosphere(0,2) = accumulatorB / T(n_search_pixels);






}





} // namespace anpi

#endif //ATMOSPHERE_HPP
