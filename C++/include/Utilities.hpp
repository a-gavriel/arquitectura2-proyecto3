#ifndef ANPI_UTILITIES_HPP
#define ANPI_UTILITIES_HPP

#include <cmath>
#include <limits>
#include <functional>
#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string.h>

#include "Exception.hpp"
#include "Matrix.hpp"

namespace anpi
{

//
// /Matrices
//

/**
 * @brief Used to swap the rows of any matrix
 *
 * @tparam T template value
 * @param A The matrix that we want to swap yours rows
 * @param row1Index Index of the row 1
 * @param row2Index Index of the row 2
 * @param start value where we want to start.
  */
template <typename T>
void swapRows(Matrix<T> &A, size_t row1Index, size_t row2Index, size_t start)
{
  if (row1Index != row2Index)
  {
    for (size_t i = start; i < A.cols(); ++i)
    {
      T temp = A[row1Index][i];
      A[row1Index][i] = A[row2Index][i];
      A[row2Index][i] = temp;
    }
  }
}

#ifdef ANPI_ENABLE_SIMD
#ifdef __AVX__

/**
 * @brief Used to swap the rows of any matrix using simd instructions.
 *
 * @tparam T template value
 * @tparam regType template value of the register.
 * @param LU Matrix to we want to swap his rows.
 * @param r1 size of row 1.
 * @param r2 size of row 2.
 */
template <typename T, typename regType>
void swapRowsSIMD(Matrix<T> &LU, size_t r1, size_t r2)
{

  // std::cout << "-\n-\n-\n" << "sizeof T: "<< sizeof(T) <<"\nsize of regType:
  // "<< sizeof(regType) <<"-\n-\n-\n"<< std::endl;

  unsigned long int regSize = sizeof(regType);

  // temporary element
  regType element;
  regType *r1ptr = reinterpret_cast<regType *>(LU[r1]);
  regType *r2ptr = reinterpret_cast<regType *>(LU[r2]);

  /// total size in bytes of a row
  long unsigned int colsXsize = (long unsigned int)(LU.cols()) * (long unsigned int)(sizeof(T));

  // size_t limit = LU.cols();
  for (unsigned long int i = 0; i < colsXsize; i += regSize)
  {
    // swaping the current element block at index i, between the rows.
    element = *r1ptr;
    *r1ptr++ = *r2ptr;
    *r2ptr++ = element;
  }
}
#endif
#endif

/**
 * @brief used to swap some rows to reduce numerical errors.
 * @tparam T template value
 * @param A matrix to we want apply pivoting process.
 * @param columnIndex index of the column to apply the pivot.
 * @param columnStart column where to start the pivoting process.
 * @param rowStart row where to start the pivoting process.
 * @param permut permutation vector.
  */
template <typename T>
void pivot(Matrix<T> &A, size_t columnIndex, size_t columnStart, size_t rowStart, std::vector<size_t> &permut)
{
  // Finds the maximun element in the first column to do the pivot
  T max = A[rowStart][columnIndex];
  size_t maxI = rowStart;
  for (size_t p = rowStart + 1; p < A.rows(); ++p)
  {
    if (std::abs(A[p][columnIndex]) > std::abs(max))
    {
      maxI = p;
      max = A[p][columnIndex];
    }
  }
  // Swaps the row in the A matrix and in the vector
  if (maxI != rowStart)
  {

#ifdef ANPI_ENABLE_SIMD
#ifdef __AVX__
    swapRowsSIMD<T, typename avx_traits<T>::reg_type>(A, rowStart, maxI);
#else
    swapRows(A, rowStart, maxI, columnStart);
#endif
#else
    swapRows(A, rowStart, maxI, columnStart);
#endif

    std::swap(permut[rowStart], permut[maxI]);
  }
}

/**
 * @brief used to print the matrix
 * @tparam T template value
 * @param m matrix to print
 * @param str string to print with matrix
   */
template <typename T>
static void matrix_show(const Matrix<T> &m, const std::string &str = "")
{
  std::cout << str << "\n";
  for (size_t i = 0; i < m.rows(); i++)
  {
    for (size_t j = 0; j < m.cols(); j++)
    {
      printf(" %8.15f", m(i, j));
    }
    printf("\n");
  }
  printf("\n");
}

// this function prints a matrix casting the values to integer of default size 3
// (max number 999)
template <typename T>
static void matrix_show_int(const Matrix<T> &m, const std::string &str = "", int maxsize = 3)
{
  std::cout << str << "\n";
  std::stringstream ss;
  for (size_t i = 0; i < m.rows(); i++)
  {
    for (size_t j = 0; j < m.cols(); j++)
    {
      ss.str("");
      ss << std::setw(maxsize) << std::setfill(' ') << (int)(m(i, j));
      std::cout << ss.str() << ' ';
      // printf(" %d",(int) m(i,j));
    }
    printf("\n");
  }
  printf("\n");
}

// this function saves a matrix casting the values to integer of default size 3
// (max number 999) saves the matrix in a file called matrix.txt
template <typename T>
static void matrix_show_file(const Matrix<T> &m, bool noVisuals, std::string name)
{

  // std::stringstream ss;

  std::ofstream myfile;
  myfile.open(name);

  if (!noVisuals)
  {
    for (size_t i = 0; i < m.rows(); i++)
    {
      for (size_t j = 0; j < m.cols(); j++)
      {
        // ss.str(""); ss << std::setw(maxsize) << std::setfill (' ') <<
        // (int)(m(i,j)); myfile << ss.str() << ' ';
        myfile << m(i, j) << ' ';
      }
      myfile << "\n";
    }
    std::cout << "Saved matix to file: " << name << "\n";
  }

  myfile << "\n";

  myfile.close();
}

// this function prints a matrix
template <typename T>
std::string pymat_row(const Matrix<T> &m, size_t i)
{
  std::string pyrow = "[";
  size_t j = 0;
  for (; j < m.cols() - 1; j++)
  {
    pyrow += std::to_string(m(i, j)) + " , ";
  }
  pyrow += std::to_string(m(i, j)) + " ]";
  return pyrow;
}

/**
 * @brief generate a identity matrix.
 * @tparam T template value.
 * @param rows number of rows.
 * @param cols number of columns
 * @return A identity matrix.
   */
template <typename T>
anpi::Matrix<T> identityMatrix(const size_t rows, const size_t cols)
{

  anpi::Matrix<T> identity(rows, cols);
  identity.fill(T(0));

  for (size_t i = 0; i < rows and i < cols; ++i)
  {
    identity(i, i) = T(1);
  }

  return identity;
}

// Reshape function

template <typename T>
static bool reshape(const Matrix<T> &m, const size_t Brows, const size_t Bcols, Matrix<T> &B)
{
  size_t Mrows = m.rows();
  size_t Mcols = m.cols();

  

  if (Mcols * Mrows == Brows * Bcols)
  {
    B.allocate(Brows, Bcols);

    size_t i = 0;
    size_t j = 0;
    for (size_t col = 0; col < Mcols; col++)
    {
      for (size_t row = 0; row < Mrows; row++)
      {
        B[i][j++] = m[row][col];
        if (j >= Bcols)
        {
          i++;
          j = 0;
        }
      }
    }
    return true;
  }
  else
  {
    return false;
  }
}

// rgb2gray function
//TODO: untested
template <typename T>
static void rgb2gray(const Matrix<T> &R, const Matrix<T> &G, const Matrix<T> &B, Matrix<T> &Gray)
{
  size_t Rrows = R.rows();
  size_t Grows = G.rows();
  size_t Brows = B.rows();
  size_t Rcols = R.cols();
  size_t Gcols = G.cols();
  size_t Bcols = B.cols();

  bool sameRows = (Rrows == Grows) && (Rrows == Brows);
  bool sameCols = (Rcols == Gcols) && (Rcols == Bcols);

  if (sameCols && sameRows)
  {
    Gray.allocate(Rrows, Rcols);
    T added;
    for (size_t row = 0; row < Rrows; row++)
    {
      for (size_t col = 0; col < Rcols; col++)
      {
        added = R[row][col] + G[row][col] + B[row][col];
        Gray[row][col] = added / T(3);
      }
    }
  }
  else
  {
    std::cout << "Flags unmet!\n"
              << (int)sameRows << (int)sameCols << std::endl;
  }
}

// computes accumulative sume given a direction
template <typename T>
static void cumsum(const Matrix<T> &A, int d, Matrix<T> &B)
{
  if (d == 1)
  {
    cumsumRow(A, B);
  }
  else
  {
    cumsumCol(A, B);
  }
}

// Compmutes accumulative sum per row
template <typename T>
static void cumsumRow(const Matrix<T> &A, Matrix<T> &B)
{
  B.allocate(A.rows(), A.cols());
  //copies first row
  for (size_t j = 0; j < A.cols(); j++)
  {
    B[0][j] = A[0][j];
  }
  //computes the accumulative sum per row
  for (size_t i = 1; i < A.rows(); i++)
  {
    for (size_t j = 0; j < A.cols(); j++)
    {
      B[i][j] = B[i - 1][j] + A[i][j];
    }
  }
}

// Compmutes accumulative sum per col
template <typename T>
static void cumsumCol(const Matrix<T> &A, Matrix<T> &B)
{
  B.allocate(A.rows(), A.cols());
  //copies first row
  for (size_t i = 0; i < A.rows(); i++)
  {
    B[i][0] = A[i][0];
  }
  //computes the accumulative sum per row
  for (size_t i = 0; i < A.rows(); i++)
  {
    for (size_t j = 1; j < A.cols(); j++)
    {
      B[i][j] = B[i][j - 1] + A[i][j];
    }
  }
}

// Moves a windows of a matrix inside a window of a matrix
// from B to A
template <typename T>
static void copyWindow(Matrix<T> &A, size_t Aistart, size_t Aiend, size_t Ajstart, size_t Ajend,
                       const Matrix<T> &B, size_t Bistart, size_t Biend, size_t Bjstart, size_t Bjend)
{

  bool sameRows = (Aiend - Aistart) == (Biend - Bistart); //both windows must be same size
  bool sameCols = (Ajend - Ajstart) == (Bjend - Bjstart);
  bool lessRows = (Aiend - Aistart) < A.rows(); // cant copy more than the size of the matrix
  bool lessCols = (Ajend - Ajstart) < A.cols();

  if (sameRows && sameCols && lessRows && lessCols)
  {
    size_t rows = Aiend - Aistart;
    size_t cols = Ajend - Ajstart;
    for (size_t i = 0; i <= rows; ++i)
    {
      for (size_t j = 0; j <= cols; ++j)
      {
        A[i + Aistart][j + Ajstart] = B[i + Bistart][j + Bjstart];
      }
    }
  }
  else
  {
    std::cout << "flags unmet!" << (int)sameRows << (int)sameCols << (int)lessRows << (int)lessCols << std::endl;
  }
}

// Moves a windows of a matrix inside a window of a matrix
// from B to A
template <typename T>
static void getWindow(
    const Matrix<T> &B, size_t Bistart, size_t Biend, size_t Bjstart, size_t Bjend, Matrix<T> &A)
{

  bool flagless = (Bistart <= Biend) && (Bjstart <= Bjend);

  bool flagrows = (Bistart >= 0) && (Biend < B.rows());
  bool flagcols = (Bjstart >= 0) && (Bjend < B.cols());

  if (flagcols && flagrows && flagless)
  {
    A.allocate(Biend - Bistart + 1, Bjend - Bjstart + 1);

    size_t Aistart = 0;
    size_t Aiend = Biend - Bistart;
    size_t Ajstart = 0;
    size_t Ajend = Bjend - Bjstart;

    size_t rows = Aiend - Aistart;
    size_t cols = Ajend - Ajstart;
    for (size_t i = 0; i <= rows; ++i)
    {
      for (size_t j = 0; j <= cols; ++j)
      {
        A[i + Aistart][j + Ajstart] = B[i + Bistart][j + Bjstart];
      }
    }
  }
  else
  {
    A.clear();
  }
}

// Moves a windows of a matrix inside a window of a matrix
// from B to A
template <typename T>
static void repmat(const Matrix<T> &A, size_t r, size_t c, Matrix<T> &B)
{

  size_t rows = A.rows();
  size_t cols = A.cols();
  size_t Nrows = rows * r;
  size_t Ncols = cols * c;
  B.allocate(Nrows, Ncols);

  for (size_t i = 0; i < Nrows; ++i)
  {
    for (size_t j = 0; j < Ncols; ++j)
    {
      B[i][j] = A[i % rows][j % cols];
    }
  }
}

template <typename T>
static T min(const Matrix<T> &A)
{

  size_t rows = A.rows();
  size_t cols = A.cols();

  T smallest = A(0, 0);
  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < cols; ++j)
    {
      if (A(i, j) < smallest)
      {
        smallest = A(i, j);
      }
    }
  }
  return smallest;
}

template <typename T>
static void maxCap(Matrix<T> &A, T min)
{
  size_t rows = A.rows();
  size_t cols = A.cols();
  for (size_t i = 0; i < rows; ++i)
  {
    for (size_t j = 0; j < cols; ++j)
    {
      if (A(i, j) < min)
      {
        A(i, j) = min;
      }
    }
  }
}

template <typename T>
static T min_abs(T A, T B, T C)
{
  T result;
  if (A < B)
  {
    result = A;
  }
  else
  {
    result = B;
  }

  if (C < result)
  {
    result = C;
  }
  return result;
}

//Converts the matrix into 2 vectors, One of values, One of indices
template <typename T>
static void vectorizeIndex(const Matrix<T> &m, std::vector<T> &B, std::vector<size_t> &indices)
{
  size_t Mrows = m.rows();
  size_t Mcols = m.cols();

  B.resize(Mrows * Mcols);
  indices.resize(Mrows * Mcols);

  size_t i = 0;
  for (size_t col = 0; col < Mcols; col++)
  {
    for (size_t row = 0; row < Mrows; row++)
    {
      B[i] = m[row][col];
      indices[i] = i;
      i++;
    }
  }
}

//Converts the matrix into 2 vectors, One of values, One of indices
template <typename T>
static void vectorize(const Matrix<T> &m, std::vector<T> &B)
{
  size_t Mrows = m.rows();
  size_t Mcols = m.cols();

  B.resize(Mrows * Mcols);

  size_t i = 0;
  for (size_t col = 0; col < Mcols; col++)
  {
    for (size_t row = 0; row < Mrows; row++)
    {
      B[i] = m[row][col];

      i++;
    }
  }
}

template <typename T>
void quickSort(std::vector<T> &arr, std::vector<size_t> &ind, long left, long right)
{
  long i = left, j = right;
  T tmp;
  size_t tmp2;
  T pivot = arr[(left + right) / 2];
  /* partition */
  while (i <= j)
  {
    while (arr[i] > pivot)
      i++;
    while (arr[j] < pivot)
      j--;
    if (i <= j)
    {
      tmp = arr[i];
      tmp2 = ind[i];
      arr[i] = arr[j];
      ind[i] = ind[j];
      arr[j] = tmp;
      ind[j] = tmp2;
      i++;
      j--;
    }
  };
  /* recursion */
  if (left < j)
    quickSort(arr, ind, left, j);
  if (i < right)
    quickSort(arr, ind, i, right);
}

template <typename T>
void sort(std::vector<T> &values, std::vector<size_t> &ind, int dir)
{
  if (dir == 1)
  {
    quickSort(values, ind, 0, values.size() - 1);
  }
}

// function prints vector
/**
 * @brief function to print a vector.
 * @tparam T template value.
 * @param vect vector to print.
 */
template <typename T>
static void vector_show(const std::vector<T> &vect)
{
  size_t size = vect.size();
  for (size_t i = 0; i < size; ++i)
  {
    std::cout << vect[i] << " ";
  }
  std::cout << "\n";
}
} // namespace anpi

#endif