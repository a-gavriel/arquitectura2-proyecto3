#ifndef TRANSMISSION_HPP
#define TRANSMISSION_HPP

#include <cstdlib>
#include <iostream>

#include <string>

#include <AnpiConfig.hpp>
#include <fstream>
#include <Exception.hpp>
#include "Utilities.hpp"
#include "omp.h"
#include "DarkChannel.hpp"
#include <algorithm>

namespace anpi
{

template <typename T>
/**
 * get_transmission_estimate. Uses atmosphere's mask and gets the estimated
 * transmission function of the hazed scene
 *
 * @param mixed   &imageR       reference of the red channel of the image
 * @param mixed   &imageG       reference of the green channel of the image
 * @param mixed   &imageB       reference of the blue channel of the image
 * @param mixed   &atmosphere    
 * @param T  omega:
 * @param size_t  win_size      window size
 * @param mixed   &trans_est    
 * @return  void
 */
void get_transmission_estimate(Matrix<T> &imageR, Matrix<T> &imageG, Matrix<T> &imageB,
                               Matrix<T> &atmosphere, T omega, size_t win_size, Matrix<T> &trans_est)
{
  size_t m = imageR.rows();
  size_t n = imageR.cols();

  /* Decomposition of the vector in 3 matrix, each with the same value as the
  dimension of the atmosphere (the first matrix will only have the first data
  replicated and so on)*/

  // aux values
  T value1 = atmosphere[0][0];
  T value2 = atmosphere[0][1];
  T value3 = atmosphere[0][2];

  // repmat
  Matrix<T> rep_atmosphere1(m, n);
  Matrix<T> rep_atmosphere2(m, n);
  Matrix<T> rep_atmosphere3(m, n);
  rep_atmosphere1.fill(value1);
  rep_atmosphere2.fill(value2);
  rep_atmosphere3.fill(value3);

  Matrix<T> aux1 = imageR / rep_atmosphere1;
  Matrix<T> aux2 = imageG / rep_atmosphere2;
  Matrix<T> aux3 = imageB / rep_atmosphere3;

  anpi::get_dark_channel(aux1, aux2, aux3, win_size, trans_est);

  // Auxiliar variables
  Matrix<T> omegaMat(m, n);
  Matrix<T> onesMat(m, n);
  omegaMat.fill(omega);
  onesMat.fill(1);

  // ^ is multiplication element by element
  trans_est = onesMat - (omegaMat ^ trans_est);
}

} // namespace anpi

#endif //TRANSMISSION_HPP
