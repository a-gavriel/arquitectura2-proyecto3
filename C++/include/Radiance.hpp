#ifndef RADIANCE_HPP
#define RADIANCE_HPP

#include <cstdlib>
#include <iostream>

#include <string>

#include <AnpiConfig.hpp>
#include <fstream>
#include <Matrix.hpp>
#include <Exception.hpp>
#include "Utilities.hpp"
#include "omp.h"
#include <algorithm>

namespace anpi
{

/**
 * get_radiance.
 *
 * @param	mixed	&imageR      	
 * @param	mixed	&imageG      	
 * @param	mixed	&imageB      	
 * @param	mixed	transmissionR	
 * @param	mixed	transmissionG	
 * @param	mixed	transmissionB	
 * @param	mixed	&atmosphere  	
 * @return	void
 */
template <typename T>
void get_radiance(Matrix<T> &imageR, Matrix<T> &imageG, Matrix<T> &imageB,
                  Matrix<T> &transmission, Matrix<T> &atmosphere, Matrix<T> &radianceR,
                  Matrix<T> &radianceG, Matrix<T> &radianceB)
{
  size_t m = imageR.rows();
  size_t n = imageR.cols();

  // aux values
  T value1 = atmosphere(0, 0);
  T value2 = atmosphere(0, 1);
  T value3 = atmosphere(0, 2);

  // repmat
  Matrix<T> rep_atmosphere1(m, n);
  Matrix<T> rep_atmosphere2(m, n);
  Matrix<T> rep_atmosphere3(m, n);
  rep_atmosphere1.fill(value1);
  rep_atmosphere2.fill(value2);
  rep_atmosphere3.fill(value3);

  // max
  maxCap<T>(transmission, 0.1);
  
  radianceR = ((imageR - rep_atmosphere1) / transmission) + rep_atmosphere1;
  radianceG = ((imageG - rep_atmosphere2) / transmission) + rep_atmosphere2;
  radianceB = ((imageB - rep_atmosphere3) / transmission) + rep_atmosphere3;
}

} // namespace anpi

#endif // RADIANCE_HPP
