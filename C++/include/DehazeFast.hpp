#ifndef DEHAZE_FAST_HPP
#define DEHAZE_FAST_HPP

#include <cstdlib>
#include <iostream>
#include <string>
#include <AnpiConfig.hpp>
#include <fstream>
#include <Matrix.hpp>
#include <Exception.hpp>
#include <algorithm>
#include <omp.h>

#include <Utilities.hpp>
#include <DarkChannel.hpp>
#include <Atmosphere.hpp>
#include <Transmission.hpp>
#include <GuidedFilter.hpp>
#include <Radiance.hpp>

namespace anpi
{
// omega= 0.9375
// winsize = 15
template <typename T>
void dehaze_fast(Matrix<T> &imageR, Matrix<T> &imageG, Matrix<T> &imageB,
                 T omega, size_t win_size, Matrix<T> &radianceR,
                 Matrix<T> &radianceG, Matrix<T> &radianceB)
{
  T radius = T(15);
  T epsilon = T(0.001);

  size_t m = imageR.rows();
  size_t n = imageR.cols();

  // Dark channel is 2D
  // atmosphere is 2D
  // trans_est is 2D
  // radiance is 3D
  Matrix<T> darkchannel, atmosphere, trans_est, transmission, X, imageGray;

  anpi::get_dark_channel(imageR, imageG, imageB, win_size, darkchannel);

  anpi::get_atmosphere(imageR, imageG, imageB, darkchannel, atmosphere);

  anpi::get_transmission_estimate(imageR, imageG, imageB, atmosphere, omega,
                                  win_size, trans_est);

  rgb2gray(imageR, imageG, imageB, imageGray);

  anpi::guided_filter(imageGray, trans_est, radius, epsilon, X);

  reshape(X, m, n, transmission);

  anpi::get_radiance(imageR, imageG, imageB, transmission, atmosphere,
                     radianceR, radianceG, radianceB);
}

} // namespace anpi

#endif // DEHAZE_FAST_HPP
