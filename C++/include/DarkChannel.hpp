#ifndef DARK_CHANNEL_HPP
#define DARK_CHANNEL_HPP

#include <cstdlib>
#include <iostream>

#include <string>

#include <AnpiConfig.hpp>
#include <fstream>
#include <Matrix.hpp>
#include <Exception.hpp>
#include "Utilities.hpp"
#include "omp.h"
#include "PlotPy.hpp"
#include <algorithm>

namespace anpi
{

/**
 * get_dark_channel. This function looks for the lowest value of pixels, the
 * dark ones. After this, it maps each patch to an image of the same dimension
 * with each minimum value
 *
 * @param mixed   &imageR         Red channel of the image
 * @param mixed   &imageG         Green channel of the image
 * @param mixed   &imageB         Blue channel of the image
 * @param size_t  win_size        Tells the square area that covers the image
 * where it looks for the minimum value pixel
 * @param mixed   &dark_channelR  Red channel after get_dark_channel
 * @param mixed   &dark_channelG  Green channel after get_dark_channel
 * @param mixed   &dark_channelB  Blue channel after get_dark_channel
 * @return  void
 */
template <typename T>
void get_dark_channel(Matrix<T> &imageR, Matrix<T> &imageG, Matrix<T> &imageB,
                      size_t win_size, Matrix<T> &dark_channel)
{
  size_t m = imageR.rows();
  size_t n = imageR.cols();

  // Divides by 2
  size_t pad_size = win_size >> 1;

  // Padded array
  Matrix<T> padded_imageR(m + (pad_size * 2), n + (pad_size * 2));
  Matrix<T> padded_imageG(m + (pad_size * 2), n + (pad_size * 2));
  Matrix<T> padded_imageB(m + (pad_size * 2), n + (pad_size * 2));
  padded_imageR.fill(std::numeric_limits<T>::infinity());
  padded_imageG.fill(std::numeric_limits<T>::infinity());
  padded_imageB.fill(std::numeric_limits<T>::infinity());
  copyWindow(padded_imageR, pad_size, padded_imageR.rows() - 1 - pad_size,
             pad_size, padded_imageR.cols() - 1 - pad_size, imageR, 0, m - 1,
             0, n - 1);
  copyWindow(padded_imageG, pad_size, padded_imageG.rows() - 1 - pad_size,
             pad_size, padded_imageG.cols() - 1 - pad_size, imageG, 0, m - 1,
             0, n - 1);
  copyWindow(padded_imageB, pad_size, padded_imageB.rows() - 1 - pad_size,
             pad_size, padded_imageB.cols() - 1 - pad_size, imageB, 0, m - 1,
             0, n - 1);

  // Resize & empty the matrix
  Matrix<T> dark_channelR(m, n);
  Matrix<T> dark_channelG(m, n);
  Matrix<T> dark_channelB(m, n);
  dark_channel.allocate(m, n);

  Matrix<T> patch(m, n);

  size_t j;
  size_t i;

#pragma omp parallel for private(i, j, patch) \
    shared(win_size, dark_channel, dark_channelB, dark_channelG, dark_channelR) \
    num_threads(omp_get_max_threads())
  for (j = 0; j < m; j++)
  {
    for (i = 0; i < n; i++)
    {
      // Gets the min of a patch in RGB and saves it un dark_channel auxiliar
      getWindow(padded_imageR, j, j + (win_size - 1), i, i + (win_size - 1), patch);
      dark_channelR(j, i) = min(patch);
      getWindow(padded_imageG, j, j + (win_size - 1), i, i + (win_size - 1), patch);
      dark_channelG(j, i) = min(patch);
      getWindow(padded_imageB, j, j + (win_size - 1), i, i + (win_size - 1), patch);
      dark_channelB(j, i) = min(patch);

      // Gets the minimum across all the RGB channels
      dark_channel(j, i) = min_abs(dark_channelR(j, i), dark_channelG(j, i), dark_channelB(j, i));
    }
  }
}

} // namespace anpi

#endif //DARK_CHANNEL_HPP
