/**
 * Copyright (C) 2017 
 * Área Académica de Ingeniería en Computadoras, TEC, Costa Rica
 *
 * This file is part of the CE3102 Numerical Analysis lecture at TEC
 *
 * @Author: Pablo Alvarado
 * @Date  : 10.02.2018
 */

#include <boost/test/unit_test.hpp>

#include "Utilities.hpp"
#include "WindowFilter.hpp"
#include "GuidedFilter.hpp"

#include <iostream>
#include <exception>
#include <cstdlib>
#include <complex>
#include <Matrix.hpp>
#include <functional>

#include <cmath>

namespace anpi
{
namespace test
{

template <typename T>
void guidedFilterTest(const std::function<void(const Matrix<T> &, size_t, Matrix<T> &)> &window_sum,
                      const std::function<void(const Matrix<T> &, const Matrix<T> &, float, float, Matrix<T> &)> &guided_filter)
{
  // The result
  Matrix<T> Blank, Extra, result;
  bool testResult;
  // Test windowfilter
  {
    Blank = {{0.701617, 0.728196, 0.278817, 0.678152, 0.390068},
             {0.755938, 0.783500, 0.132573, 0.493890, 0.724498},
             {0.846498, 0.107968, 0.567178, 0.663084, 0.369149},
             {0.984995, 0.721426, 0.139885, 0.528728, 0.189438},
             {0.099112, 0.600018, 0.620375, 0.513188, 0.647545}};

    result.clear();

    window_sum(Blank, 2, result);

    //matrix_show(result, "");

    Matrix<T> expected = {{4.9023, 6.7374, 8.2211, 5.9171, 4.2974},
                          {6.7486, 9.1124, 10.7856, 7.4965, 5.1555},
                          {8.0681, 10.9451, 13.2658, 9.8777, 6.9366},
                          {6.3595, 8.5584, 10.4890, 7.8024, 5.5895},
                          {4.6875, 6.3925, 7.5986, 5.6680, 4.2386}};

    testResult = true;
    for (size_t i = 0; i < result.rows(); ++i)
    {
      for (size_t j = 0; j < result.cols(); ++j)
      {
        testResult = testResult && (std::abs(expected(i, j) - result(i, j)) < 0.001);
      }
    }
    BOOST_CHECK(testResult);
  }

  // Test Guided filter
  {
    Blank = {{5, 10, 5, 10, 5, 10, 5, 10},
             {20, 30, 20, 30, 20, 30, 20, 30},
             {45, 60, 45, 60, 45, 60, 45, 60},
             {55, 75, 55, 75, 55, 75, 55, 75},
             {75, 100, 75, 100, 75, 100, 75, 100},
             {105, 135, 105, 135, 105, 135, 105, 135},
             {120, 155, 120, 155, 120, 155, 120, 155},
             {145, 185, 145, 185, 145, 185, 145, 185},
             {180, 225, 180, 225, 180, 225, 180, 225}};

    Extra = {{2.2361, 3.1623, 2.2361, 3.1623, 2.2361, 3.1623, 2.2361, 3.1623},
             {4.4721, 5.4772, 4.4721, 5.4772, 4.4721, 5.4772, 4.4721, 5.4772},
             {6.7082, 7.7460, 6.7082, 7.7460, 6.7082, 7.7460, 6.7082, 7.7460},
             {7.4162, 8.6603, 7.4162, 8.6603, 7.4162, 8.6603, 7.4162, 8.6603},
             {8.6603, 10.0000, 8.6603, 10.0000, 8.6603, 10.0000, 8.6603, 10.0000},
             {10.2470, 11.6190, 10.2470, 11.6190, 10.2470, 11.6190, 10.2470, 11.6190},
             {10.9545, 12.4499, 10.9545, 12.4499, 10.9545, 12.4499, 10.9545, 12.4499},
             {12.0416, 13.6015, 12.0416, 13.6015, 12.0416, 13.6015, 12.0416, 13.6015},
             {13.4164, 15.0000, 13.4164, 15.0000, 13.4164, 15.0000, 13.4164, 15.0000}};

    result.clear();

    guided_filter(Blank, Extra, T(2), T(0.5), result);

    //matrix_show(result, "");

    Matrix<T> expected = {{2.8058, 3.2771, 2.8216, 3.2924, 2.8453, 3.3117, 2.8635, 3.3246},
                          {4.3775, 5.2189, 4.3864, 5.2233, 4.3996, 5.2298, 4.4102, 5.2340},
                          {6.5647, 7.7162, 6.5640, 7.7070, 6.5626, 7.6975, 6.5626, 7.6908},
                          {7.3541, 8.6870, 7.3544, 8.6784, 7.3545, 8.6697, 7.3557, 8.6635},
                          {8.6501, 10.0736, 8.6503, 10.0653, 8.6502, 10.0569, 8.6513, 10.0510},
                          {10.2112, 11.6710, 10.2111, 11.6634, 10.2106, 11.6556, 10.2113, 11.6502},
                          {10.9125, 12.4558, 10.9137, 12.4501, 10.9151, 12.4445, 10.9172, 12.4406},
                          {11.9720, 13.6511, 11.9714, 13.6430, 11.9701, 13.6345, 11.9702, 13.6285},
                          {13.3714, 15.1730, 13.3683, 15.1620, 13.3633, 15.1497, 13.3605, 15.1413}};

    testResult = true;
    for (size_t i = 0; i < result.rows(); ++i)
    {
      for (size_t j = 0; j < result.cols(); ++j)
      {
        testResult = testResult && (std::abs(expected(i, j) - result(i, j)) < 0.27);
      }
    }
    BOOST_CHECK(testResult);
  }
}

} // namespace test
} // namespace anpi

BOOST_AUTO_TEST_SUITE(GUIDEDFILTER)
BOOST_AUTO_TEST_CASE(guidedFilterTest)
{
  anpi::test::guidedFilterTest<double>(anpi::window_sum<double>, anpi::guided_filter<double>);
}

BOOST_AUTO_TEST_SUITE_END()
