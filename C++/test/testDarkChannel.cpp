#include <boost/test/unit_test.hpp>

#include "Utilities.hpp"
#include "DarkChannel.hpp"
#include <iostream>
#include <exception>
#include <cstdlib>
#include <complex>

#include <functional>

#include <cmath>

namespace anpi
{
namespace test
{
template <typename T>
void getDarkChannelTest(const std::function<void(Matrix<T> &, Matrix<T> &, Matrix<T> &,
                                                 size_t, Matrix<T> &)> &get_dark_channel)
{
  // Inputs
  Matrix<T> imageR = {{0.798446, 0.995372, 0.039460, 0.681495, 0.846647},
                      {0.254566, 0.221957, 0.279784, 0.538639, 0.031434},
                      {0.195377, 0.875261, 0.325294, 0.492575, 0.110767},
                      {0.512909, 0.729523, 0.297202, 0.202832, 0.580592},
                      {0.865646, 0.992923, 0.564144, 0.998035, 0.071844}};
  Matrix<T> imageG = {{0.322547, 0.552469, 0.442065, 0.474528, 0.233746},
                      {0.877118, 0.988008, 0.581899, 0.984812, 0.500955},
                      {0.225315, 0.364839, 0.027887, 0.632170, 0.784134},
                      {0.648070, 0.956190, 0.581795, 0.708566, 0.874919},
                      {0.249518, 0.468029, 0.706980, 0.316573, 0.050267}};
  Matrix<T> imageB = {{0.529670, 0.353769, 0.569743, 0.542989, 0.610149},
                      {0.955630, 0.911118, 0.617452, 0.660118, 0.094671},
                      {0.284356, 0.887544, 0.240471, 0.855563, 0.653003},
                      {0.935794, 0.468945, 0.796673, 0.651254, 0.206742},
                      {0.275157, 0.219169, 0.962937, 0.848168, 0.583852}};

  size_t win_size = 3;

  Matrix<T> dark_channel;

  get_dark_channel(imageR, imageG, imageB, win_size,
                   dark_channel);

  // Outputs
  Matrix<T> Result = {{0.221957, 0.039460, 0.039460, 0.031434, 0.031434},
                      {0.195377, 0.027887, 0.027887, 0.027887, 0.031434},
                      {0.195377, 0.027887, 0.027887, 0.027887, 0.031434},
                      {0.195377, 0.027887, 0.027887, 0.027887, 0.050267},
                      {0.219169, 0.219169, 0.202832, 0.050267, 0.050267}};

  bool testResult = true;
  for (size_t i = 0; i < imageB.rows(); ++i)
  {
    for (size_t j = 0; j < imageB.cols(); ++j)
    {
      testResult = testResult && (std::abs(dark_channel(i, j) - Result(i, j)) < 0.001);
    }
  }

  std::cout << "Difference Dark Channel" << std::endl;
  matrix_show(dark_channel - Result);

  BOOST_CHECK(testResult);
}
} // namespace test
} // namespace anpi

BOOST_AUTO_TEST_SUITE(DARKCHANNEL)
BOOST_AUTO_TEST_CASE(getDarkChannel)
{
  anpi::test::getDarkChannelTest<float>(anpi::get_dark_channel<float>);
}

BOOST_AUTO_TEST_SUITE_END()
