#include <boost/test/unit_test.hpp>

#include "Utilities.hpp"
#include "Radiance.hpp"
#include <iostream>
#include <exception>
#include <cstdlib>
#include <complex>

#include <functional>

#include <cmath>

namespace anpi
{
namespace test
{
template <typename T>
void getRadianceTest(const std::function<void(Matrix<T> &, Matrix<T> &, Matrix<T> &,
                                              Matrix<T> &, Matrix<T> &, Matrix<T> &,
                                              Matrix<T> &, Matrix<T> &)>
                         &get_radiance)
{
  // Inputs
  Matrix<T> imageR = {{0.798446, 0.995372, 0.039460, 0.681495, 0.846647},
                      {0.254566, 0.221957, 0.279784, 0.538639, 0.031434},
                      {0.195377, 0.875261, 0.325294, 0.492575, 0.110767},
                      {0.512909, 0.729523, 0.297202, 0.202832, 0.580592},
                      {0.865646, 0.992923, 0.564144, 0.998035, 0.071844}};
  Matrix<T> imageG = {{0.322547, 0.552469, 0.442065, 0.474528, 0.233746},
                      {0.877118, 0.988008, 0.581899, 0.984812, 0.500955},
                      {0.225315, 0.364839, 0.027887, 0.632170, 0.784134},
                      {0.648070, 0.956190, 0.581795, 0.708566, 0.874919},
                      {0.249518, 0.468029, 0.706980, 0.316573, 0.050267}};
  Matrix<T> imageB = {{0.529670, 0.353769, 0.569743, 0.542989, 0.610149},
                      {0.955630, 0.911118, 0.617452, 0.660118, 0.094671},
                      {0.284356, 0.887544, 0.240471, 0.855563, 0.653003},
                      {0.935794, 0.468945, 0.796673, 0.651254, 0.206742},
                      {0.275157, 0.219169, 0.962937, 0.848168, 0.583852}};
  Matrix<T> transmission = {{0.73385, 0.95268, 0.95268, 0.96231, 0.96231},
                            {0.76572, 0.96538, 0.96538, 0.96538, 0.96231},
                            {0.76572, 0.96538, 0.96538, 0.96538, 0.96231},
                            {0.76572, 0.96538, 0.96538, 0.96538, 0.93760},
                            {0.73543, 0.73543, 0.75678, 0.93760, 0.93760}};
  Matrix<T> atmosphere = {{0.83395, 0.80559, 0.82840}};

  Matrix<T> radianceR;
  Matrix<T> radianceG;
  Matrix<T> radianceB;

  get_radiance(imageR, imageG, imageB, transmission, atmosphere, radianceR, radianceG, radianceB);

  Matrix<T> ResultR = {{0.78557, 1.00339, 0.00000, 0.67552, 0.84714},
                       {0.07730, 0.20001, 0.25991, 0.52805, -0.00000},
                       {0.00000, 0.87674, 0.30705, 0.48033, 0.08244},
                       {0.41468, 0.72578, 0.27796, 0.18020, 0.56373},
                       {0.87705, 1.05011, 0.47743, 1.00895, 0.02113}};
  Matrix<T> ResultG = {{0.14736, 0.53990, 0.42401, 0.46156, 0.21135},
                       {0.89900, 0.99455, 0.57388, 0.99124, 0.48902},
                       {0.04777, 0.34903, 0.00000, 0.62595, 0.78329},
                       {0.59988, 0.96159, 0.57377, 0.70509, 0.87953},
                       {0.04947, 0.34659, 0.67529, 0.28403, -0.00000}};
  Matrix<T> ResultB = {{4.2133e-01, 3.3020e-01, 5.5690e-01, 5.3181e-01, 6.0160e-01},
                       {9.9456e-01, 9.1408e-01, 6.0989e-01, 6.5408e-01, 6.5931e-02},
                       {1.1790e-01, 8.8966e-01, 2.1939e-01, 8.5654e-01, 6.4613e-01},
                       {9.6865e-01, 4.5606e-01, 7.9554e-01, 6.4490e-01, 1.6537e-01},
                       {7.6130e-02, -1.1102e-16, 1.0062e+00, 8.4948e-01, 5.6758e-01}};
  bool testResult = true;
  for (size_t i = 0; i < imageB.rows(); ++i)
  {
    for (size_t j = 0; j < imageB.cols(); ++j)
    {
      testResult = testResult && (std::abs(radianceR(i, j) - ResultR(i, j)) < 0.0001);
      testResult = testResult && (std::abs(radianceG(i, j) - ResultG(i, j)) < 0.0001);
      testResult = testResult && (std::abs(radianceB(i, j) - ResultB(i, j)) < 0.0001);
    }
  }
  std::cout << "Difference RadianceR" << std::endl;
  matrix_show(radianceR - ResultR);

  std::cout << "Difference RadianceG" << std::endl;
  matrix_show(radianceG - ResultG);

  std::cout << "Difference RadianceB" << std::endl;
  matrix_show(radianceB - ResultB);

  BOOST_CHECK(testResult);
}
} // namespace test
} // namespace anpi

BOOST_AUTO_TEST_SUITE(RADIANCE)
BOOST_AUTO_TEST_CASE(getRadiance)
{
  anpi::test::getRadianceTest<float>(anpi::get_radiance<float>);
}

BOOST_AUTO_TEST_SUITE_END()