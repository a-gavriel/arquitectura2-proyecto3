/**
 * Copyright (C) 2017 
 * Área Académica de Ingeniería en Computadoras, TEC, Costa Rica
 *
 * This file is part of the CE3102 Numerical Analysis lecture at TEC
 *
 * @Author: Pablo Alvarado
 * @Date  : 10.02.2018
 */

#include <boost/test/unit_test.hpp>

#include "Utilities.hpp"

#include <iostream>
#include <exception>
#include <cstdlib>
#include <complex>

#include <functional>

#include <cmath>

namespace anpi
{
namespace test
{

template <typename T>
void copyWindowTest(const std::function<void(Matrix<T> &, size_t, size_t, size_t, size_t,
                                             const Matrix<T> &, size_t, size_t, size_t, size_t)> &copyWindow,
                    const std::function<void(const Matrix<T> &, size_t, size_t, size_t, size_t, Matrix<T> &)> &getWindow,
                    const std::function<void(const Matrix<T> &, size_t, size_t, Matrix<T> &)> &repmat)
{

  // The result
  Matrix<T> Blank;

  bool testResult;

  // Test copyWindow
  {
    Blank = {{0, 0, 0, 0},
             {0, 0, 0, 0},
             {0, 0, 0, 0},
             {0, 0, 0, 0},
             {0, 0, 0, 0}};

    Matrix<T> A = {{1, 1, 1},
                   {1, 1, 1}};

    copyWindow(Blank, 0, 1, 1, 3, A, 0, 1, 0, 2);

    //matrix_show_int(Blank, "", 3);

    Matrix<T> result = {{0, 1, 1, 1},
                        {0, 1, 1, 1},
                        {0, 0, 0, 0},
                        {0, 0, 0, 0},
                        {0, 0, 0, 0}};

    testResult = true;
    for (size_t i = 0; i < Blank.rows(); ++i)
    {
      for (size_t j = 0; j < Blank.cols(); ++j)
      {
        testResult = testResult && (std::abs(result(i, j) - Blank(i, j)) < 0.001);
      }
    }

    BOOST_CHECK(testResult);
  }

  // Test copyWindow inside an infinity matrix
  {
    Blank = {{0, 0},
             {0, 0}};

    size_t m = Blank.rows();
    size_t n = Blank.cols();
    size_t pad_size = 2;
    Matrix<T> infMatR(m + (pad_size * 2), n + (pad_size * 2));
    infMatR.fill(std::numeric_limits<T>::infinity());

    copyWindow(infMatR, pad_size, infMatR.rows() - pad_size - 1,
               pad_size, infMatR.cols() - pad_size - 1, Blank, 0, m - 1,
               0, n - 1);

    //matrix_show(infMatR, "");

    Matrix<T> result = infMatR;

    result(2, 2) = T(0);
    result(2, 3) = T(0);
    result(3, 2) = T(0);
    result(3, 3) = T(0);

    testResult = true;
    for (size_t i = 0; i < Blank.rows(); ++i)
    {
      for (size_t j = 0; j < Blank.cols(); ++j)
      {
        testResult = testResult && (std::abs(result(i, j) - Blank(i, j)) < 0.001);
      }
    }

    BOOST_CHECK(true);
  }

  // Test getWindow
  {
    Blank = {{0, 1, 1, 1},
             {0, 1, 1, 1},
             {0, 0, 0, 0},
             {0, 0, 0, 0},
             {0, 0, 0, 0}};

    Matrix<T> result;

    getWindow(Blank, 0, 1, 1, 3, result);

    //matrix_show_int(result, "", 3);

    Matrix<T> A = {{1, 1, 1},
                   {1, 1, 1}};

    testResult = true;
    for (size_t i = 0; i < result.rows(); ++i)
    {
      for (size_t j = 0; j < result.cols(); ++j)
      {
        testResult = testResult && (std::abs(result(i, j) - A(i, j)) < 0.001);
      }
    }

    BOOST_CHECK(testResult);
  }

  // Test repmat
  {
    Blank = {{1, 2, 2},
             {3, 4, 4}};

    Matrix<T> result;
    repmat(Blank, 2, 3, result);
    // matrix_show_int(result, "", 3);

    Matrix<T> expected = {{1, 2, 2, 1, 2, 2, 1, 2, 2},
                          {3, 4, 4, 3, 4, 4, 3, 4, 4},
                          {1, 2, 2, 1, 2, 2, 1, 2, 2},
                          {3, 4, 4, 3, 4, 4, 3, 4, 4}};

    testResult = true;
    for (size_t i = 0; i < result.rows(); ++i)
    {
      for (size_t j = 0; j < result.cols(); ++j)
      {
        testResult = testResult && (std::abs(expected(i, j) - result(i, j)) < 0.001);
      }
    }

    BOOST_CHECK(testResult);
  }

} //copyWindowtest

template <typename T>
void cumSumTest(const std::function<void(const Matrix<T> &, Matrix<T> &)> &cumsumRow,
                const std::function<void(const Matrix<T> &, Matrix<T> &)> &cumsumCol)
{

  // The result
  Matrix<T> Blank;
  bool testResult;
  // Test testcumsumrow
  {
    Blank = {{1, 2, 1, 2},
             {3, 4, 3, 4},
             {5, 6, 5, 6},
             {2, 3, 2, 3}};

    Matrix<T> result;

    cumsumRow(Blank, result);

    //matrix_show_int(Blank, "", 3);

    Matrix<T> expected = {{1, 2, 1, 2},
                          {4, 6, 4, 6},
                          {9, 12, 9, 12},
                          {11, 15, 11, 15}};

    testResult = true;
    for (size_t i = 0; i < result.rows(); ++i)
    {
      for (size_t j = 0; j < result.cols(); ++j)
      {
        testResult = testResult && (std::abs(expected(i, j) - result(i, j)) < 0.001);
      }
    }

    BOOST_CHECK(testResult);
  }

  // Test testcumsumcol
  {
    Blank = {{1, 2, 1, 2},
             {3, 4, 3, 4},
             {5, 6, 5, 6},
             {2, 3, 2, 3}};

    Matrix<T> result;

    cumsumCol(Blank, result);

    //matrix_show_int(Blank, "", 3);

    Matrix<T> expected = {{1, 3, 4, 6},
                          {3, 7, 10, 14},
                          {5, 11, 16, 22},
                          {2, 5, 7, 10}};

    testResult = true;
    for (size_t i = 0; i < result.rows(); ++i)
    {
      for (size_t j = 0; j < result.cols(); ++j)
      {
        testResult = testResult && (std::abs(expected(i, j) - result(i, j)) < 0.001);
      }
    }

    BOOST_CHECK(testResult);
  }
}

template <typename T>
void reshapeTest(const std::function<void(const Matrix<T> &, const size_t, const size_t, Matrix<T> &)> &rehsape)
{

  // The result
  Matrix<T> Blank;
  bool testResult;
  // Test testcumsumrow
  {
    Blank = {{1, 2},
             {3, 4},
             {5, 6}};

    Matrix<T> result;

    reshape(Blank, 2, 3, result);

    //matrix_show_int(result, "", 3);

    Matrix<T> expected = 
    //{{1, 2, 3}, {4, 5, 6}};
    {{1, 3, 5}, {2, 4, 6}};

    testResult = true;
    for (size_t i = 0; i < result.rows(); ++i)
    {
      for (size_t j = 0; j < result.cols(); ++j)
      {
        testResult = testResult && (std::abs(expected(i, j) - result(i, j)) < 0.001);
      }
    }

    BOOST_CHECK(testResult);
  }
}

template <typename T>
void minTest(const std::function<T(const Matrix<T> &)> &min,
            const std::function<void( std::vector<T>&, std::vector<size_t> &, int)> &sort )


{

  // The result
  Matrix<T> Blank;
  bool testResult;
  // Test min
  {
    Blank = {{1, 2},
             {3, 0},
             {5, 6}};

    T result = min(Blank);

    T expected = T(0);

    testResult = (std::abs(expected - result) < 0.001);

    BOOST_CHECK(testResult);
  }

  // test sort
  {
    Matrix<T> Base = 
    {{ 4, 2, 6,0,7, 9,9, 2, 4, 2, 6, 9,9, 2 }};
    //{{1.9659,1.1344,4.6253,6.6580,1.9659,8.6079, 8.6079,8.6079,4.6253,8.5489,6.6096,4.1887}};


    std::vector<T> result1;
    std::vector<size_t> result2;

    //Converts the matrix into 2 vectors, One of values, One of indices
    vectorizeIndex(Base,result1, result2);


    sort(result1, result2, 1);

    //vector_show(result1);

    std::vector<T> expected = {9,9,9,9,7,6,6,4,4,2,2,2,2,0};
  

    testResult = true;

    for (size_t j = 0; j < result1.size(); ++j)
    {
      testResult = testResult && (std::abs(expected[j] - result1[j]) < 0.001);
    }
  

    BOOST_CHECK(testResult);




  }


}




template <typename T>
void grayTest(const std::function<void(Matrix<T> &, Matrix<T> &, Matrix<T> &,  Matrix<T> &)> &rgb2gray)
{
  // The result
  Matrix<T> R, G, B;
  bool testResult;
  // Test min
  {
    R = {{1, 2}, {3, 0},  {5, 6}};
    G = {{1, 2}, {3, 0},  {5, 6}};
    B = {{97, 2}, {3, 0},  {5, 6}};



    Matrix<T> result;
    rgb2gray(R,G,B,result);

    testResult = true;

    Matrix<T> expected = {{33, 2}, {3, 0},  {5, 6}};;

    for (size_t i = 0; i < result.rows(); ++i)
    {
      for (size_t j = 0; j < result.cols(); ++j)
      {
        testResult = testResult && (std::abs(expected(i, j) - result(i, j)) < 0.001);
      }
    }

    BOOST_CHECK(testResult);
  }




}












} // namespace test
} // namespace anpi

BOOST_AUTO_TEST_SUITE(UTILITIES)
BOOST_AUTO_TEST_CASE(cpwindow)
{
  anpi::test::copyWindowTest<float>(anpi::copyWindow<float>, anpi::getWindow<float>, anpi::repmat<float>);
}

BOOST_AUTO_TEST_CASE(cumsum)
{
  anpi::test::cumSumTest<float>(anpi::cumsumRow<float>, anpi::cumsumCol<float>);
}
BOOST_AUTO_TEST_CASE(reshap)
{
  anpi::test::reshapeTest<float>(anpi::reshape<float>);
}
BOOST_AUTO_TEST_CASE(minMat)
{
  anpi::test::minTest<float>(anpi::min<float>,anpi::sort<float>);
}

BOOST_AUTO_TEST_CASE(RGB2GRAY)
{
  anpi::test::grayTest<float>(anpi::rgb2gray<float>);
}


BOOST_AUTO_TEST_SUITE_END()
