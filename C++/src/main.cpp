/**
 * Copyright (C) 2018
 * Área Académica de Ingeniería en Computadoras, TEC, Costa Rica
 *
 * This file is part of the CE3102 Numerical Analysis lecture at TEC
 *
 * @Author: Andres Ramirez-Quiros
 * @Date  : 07.11.2018
 */

#include <string>
#include <vector>
#include <iterator>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <AnpiConfig.hpp>
#include <fstream>
#include <Matrix.hpp>
#include <Exception.hpp>
#include <algorithm>
#include <AnpiConfig.hpp>
#include <Exception.hpp>
#include <fstream>

#include "Utilities.hpp"
#include "omp.h"
#include "DehazeFast.hpp"

int main(int argc, const char *argv[])
{
  anpi::Matrix<float> ImageR(3000, 4000);
  anpi::Matrix<float> ImageG(3000, 4000);
  anpi::Matrix<float> ImageB(3000, 4000);
  anpi::Matrix<float> RadianceR, RadianceG, RadianceB;
  int i, k;
  float value, omega = 1;
  size_t win_size = 5;

  std::ifstream inR("../resources/red.txt");
  std::string line;
  i = 0;
  k = 0;
  while (std::getline(inR, line))
  {
    k = 0;
    std::stringstream ss(line);
    while (ss >> value)
    {
      ImageR[i][k] = value;
      ++k;
    }
    ++i;
  }

  std::ifstream inG("../resources/green.txt");
  line = "";
  i = 0;
  k = 0;
  while (std::getline(inG, line))
  {
    k = 0;
    std::stringstream ss(line);
    while (ss >> value)
    {
      ImageG[i][k] = value;
      ++k;
    }
    ++i;
  }

  std::ifstream inB("../resources/blue.txt");
  line = "";
  i = 0;
  k = 0;
  while (std::getline(inB, line))
  {
    k = 0;
    std::stringstream ss(line);
    while (ss >> value)
    {
      ImageB[i][k] = value;
      ++k;
    }
    ++i;
  }

  anpi::dehaze_fast(ImageR, ImageG, ImageB, omega, win_size,
                    RadianceR, RadianceG, RadianceB);
                    
  anpi::matrix_show_file(RadianceR, false, "resultR.txt");
  anpi::matrix_show_file(RadianceG, false, "resultG.txt");
  anpi::matrix_show_file(RadianceB, false, "resultB.txt");
}
