# ANPI

Create a directory build:

```bash
mkdir build;
```

Go into that directory

```bash
cd build;
```

You can choose to build a release version with:

```bash
cmake ../ -DCMAKE_BUILD_TYPE=Release
```

or a debug version with

```bash
cmake ../ -DCMAKE_BUILD_TYPE=Debug
```

And build everything with

```bash
make
```

\*_Se debe contar con un procesador que soporte la tecnología AVX2_

## Comandos

Desde la carpeta build se pueden realizar varias tareas:

- Para utilizar la interfaz mediante consola, se utilizan los flags asignados en
  la documentación (también se puede ver usando --help):

  ```bash
  cd bin
  ./proyecto3 --help
  ```

- También, se cuentan con archivos con perfiles de temperatura predefinidos que puede ser llamados de la forma:

  ```bash
  ./proyecto3 -p ../../data/perfil.txt
  ```

  \*_En caso que el archivo de perfil no cargue, revise el path de este o lo puede
  probar manualmente_

- Esto sirve para obtener los valores de temperatura en la placa. Para observar
  las gráficas de ellas, se corre únicamente el siguiente comandos puesto que las
  demás banderas definidas con anterioridad han sido guardadas para este proceso.

  ```bash
  ./benchmark -t thermalGraph
  ```
